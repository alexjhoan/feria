import {gql} from "@apollo/client";
import {
  APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST,
  APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST,
} from "./constants";

const _PERSIST_ = true;
const _NAME_ = "SavedFilters";

const typename =
  (_PERSIST_
    ? APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST
    : APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST) + _NAME_;

export const TYPENAME_GENERIC_SAVED_FILTER =
  typename + "_" + "generic_saved_filter";

const initialState = {
  operation_type_id: 1,
  property_type_id: [1, 2],
  season: "diciembre",
  dateFrom: null,
  dateTo: null,
  locations: [],

  __typename: typename,
};

export const QUERY_SAVED_FILTERS = gql`
query SAVED_FILTERS_QUERY{
    ${typename} @client  {
      operation_type_id
      property_type_id
      season
      dateFrom
      dateTo
    }
}`;
export const QUERY_LOCATIONS_SAVE = gql`
  query SAVED_LOCATIONS_QUERY{
      ${typename} @client  {
        locations{
          ... on Estate {
            id
            name
          }
          ... on Neighborhood {
            id
            name
            estate {
              id
              name
            }
          }
          __typename
        }
      }
  }`;

export const TYPENAME_SAVED_FILTERS = typename;
export default {
  typename,
  initialState: { [typename]: initialState },
  customQueries: {},
  mutations: {},
};
