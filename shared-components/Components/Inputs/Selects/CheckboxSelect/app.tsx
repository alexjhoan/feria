import React, { FC } from "react";
import { CheckBox } from "../../Checkbox/app";
import { Button } from "../../Button/app";
import { useCheckboxSelects, CheckboxSelectProps } from "./CheckboxSelect.hook";
import { View, Text } from 'react-native';

export const CheckboxSelect: FC<CheckboxSelectProps> = (props) => {
  const {
    isSelected,
    handleSelectOption,
    customKey,
    valueKey,
    options,
    status,
    type,
    inline,
    showMoreText,
    maxOptions,
    enableToggleOptions,
    toggleOptions,
  } = useCheckboxSelects(props);

  return (
      <View
        // className={
        //   "select-container " +
        //   type +
        //   (status ? " " + status : "") +
        //   (inline ? " inline" : "")
        // }
      >
        {options.map((opt, i) => {
          if (i >= maxOptions) return null;
          return (
            <View
              className="checkbox_select_option"
              key={"checkbox_select_option_" + i + "_" + opt[customKey]}
            >
              <CheckBox
                checked={isSelected(opt[customKey])}
                text={opt[valueKey]}
                type={type}
                handleClick={() => handleSelectOption(opt[customKey])}
              />
            </View>
          );
        })}
        {enableToggleOptions && (
          <Button
            content={showMoreText}
            type="text"
            handleClick={toggleOptions}
          />
        )}
      </View>
    
  );
};

  {/* <style jsx>{`
        .inline {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
          align-items: center;
        }
        .inline .checkbox_option {
          flex-basis: calc(
            ${100 / options.length}% - ${8 * (options.length - 1)}px
          );
        }
        .readonly {
          cursor: default;
          pointer-events: none;
          touch-action: none;
          user-select: none;
        }
        .disabled {
          cursor: default;
          pointer-events: none;
          touch-action: none;
          user-select: none;
          opacity: 0.5;
        }
      `}</style> */}