import React, { FC, useState } from "react";
// import { Label } from "../../Label/web";
import { Text, View, StyleSheet, Picker } from "react-native";
import { SelectProps, useSelects } from "../Selects.hook";

// Ignore log notification by message warning:
console.disableYellowBox = true;

export interface NativeSelectProps extends SelectProps {
	type?: "single";
}

export const Select: FC<NativeSelectProps> = (props) => {
	const {
		nvalue,
		handleSelectOption,
		customKey,
		valueKey,
		placeholder,
		options,
	} = useSelects(props);
	const Space = () => <View style={style.space} />;

	return (
		<>
			<View style={style.select}>
				<Picker
					selectedValue={nvalue}
					style={style.picker}
					onValueChange={(itemValue) => handleSelectOption(itemValue)}>
					<Picker.Item
						key={"native_option_default"}
						label={placeholder}
						value={""}
					/>
					{options.map((opt, i) => {
						return (
							<Picker.Item
								label={opt[valueKey]}
								value={opt[customKey]}
								key={"native_option_" + "_" + opt[customKey] + "_" + i}
							/>
						);
					})}
				</Picker>
				<Space />
			</View>
		</>
	);
};

const style = StyleSheet.create({
	select: {
		height: "auto",
		width: "auto",
		position: "relative",
		flexDirection: "row",
		alignItems: "center",
	},
	picker: {
		paddingVertical: 15,
		width: "100%",
		height: "100%",
	},
	space: { marginVertical: 5 },
});
