import React, { FC, useEffect } from "react";
import { Text, StyleSheet, View } from "react-native";
import { CheckBoxProps, useCheckbox } from "./Checkbox.hook";
import { Icon } from "../../Icon/app";
import { IconNames } from "../../Icon/Icon.hook";
import theme from "../../../Styles/theme";
import {
	TouchableOpacity,
	TouchableHighlight,
} from "react-native-gesture-handler";

export const CheckBox: FC<CheckBoxProps> = ({
	checked,
	text,
	handleClick,
	type = "checkbox",
	disabled,
	readonly,
}: CheckBoxProps) => {
	const { color, icon } = useCheckbox({ checked: checked, type: type });
	const textComponent = text ? <Text style={styles.text}>{text}</Text> : <></>;

	const Check = (
		<View style={styles.checkbox}>
			{checked ? <Icon name={icon} color={color} size={18} /> : <></>}
		</View>
	);

	const Radio = <Icon name={icon} color={color} />;

	return (
		<>
			<TouchableOpacity onPress={handleClick} disabled={disabled}>
				<View style={[styles.wrapper, { opacity: disabled ? 0.5 : 1 }]}>
					{type == "checkbox" ? Check : Radio}
					{textComponent}
				</View>
			</TouchableOpacity>
		</>
	);
};

const styles = StyleSheet.create({
	wrapper: {
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center",
	},
	text: {
		marginLeft: 10,
	},
	checkbox: {
		alignItems: "center",
		justifyContent: "center",
		borderRadius: 6,
		borderColor: theme.colors.textLighter,
		borderStyle: "solid",
		borderWidth: 1,
		width: 28,
		height: 28,
	},
});
