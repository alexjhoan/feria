import {FC} from 'react';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {View, Text, StyleSheet} from 'react-native';
import {Icon} from '../../../../Icon/app';
import {
  useSmartDropdownOption,
  SmartDropdownOptionProps,
} from './SmartDropdownOption.hook';
import {theme} from '../../../../../Styles';

export const SmartDropdownOption: FC<SmartDropdownOptionProps> = (props) => {
  const {
    data,
    onSelect,
    valueKey,
    groupKey,
    boldMatch,
    match,
  } = useSmartDropdownOption(props);

  /* bold match with string */
  const boldTextMatch = (string: string) => {
    return string;
    if (!boldMatch) return string;

    return string.replace(
      new RegExp(match.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, ''), 'gi'),
      function (a, b) {
        return '<b>' + a + '</b>';
      },
    );
  };
  /* end bold match with string */

  return (
    <>
      <TouchableOpacity onPress={() => onSelect(data)}>
        <View style={[styles.option, styles.flex]}>
          <View style={styles.flex}>
            {data['icon'] && (
              <View style={{marginRight: 5}}>
                <Icon
                  name={data['icon']}
                  size={16}
                  color={theme.colors.textLight}
                />
              </View>
            )}
            <Text style={styles.label}>{boldTextMatch(data[valueKey])}</Text>
          </View>
          <Text style={styles.group}>{data[groupKey]}</Text>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  option: {
    paddingHorizontal: 10,
    paddingVertical: 12,
    backgroundColor: theme.colors.background,
    justifyContent: 'space-between',
  },
  label: {
    fontSize: theme.fontSizes.appText,
    color: theme.colors.text,
  },
  group: {
    fontSize: theme.fontSizes.text,
    color: theme.colors.textLight,
  },
});
