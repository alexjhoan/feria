import { Skeleton, Typography } from "antd";

import { useSEO } from "../hook";
import { useTheme } from "../../../Styles/ThemeHook";

const { Text } = Typography;

export const SeoText = () => {
	const { seoMetaTags, error, loading } = useSEO();
	const { theme } = useTheme();

	if (loading) return <Skeleton active />;
	if (error || !seoMetaTags?.text) return null;

	return (
		<Text style={{ fontSize: theme.fontSizes.smFontSize }} type={"secondary"}>
			{seoMetaTags.text}
		</Text>
	);
};
