export interface seoMetaTags {
	url: string;
	title: string;
	h1: string;
	breadcrumbs: [
		{
			text: string;
			url: string;
		}
	];
	lastSearch?: {
		text: string;
		url: string;
	};
	description: string;
	og_image: string;
	noindex: boolean;
	canonical: string;
	site_name: string;
	links: [
		[
			{
				text: string;
				url: string;
			}
		]
	];
	text?: string;
}
