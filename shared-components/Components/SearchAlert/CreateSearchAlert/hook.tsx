import { useEffect, useState } from "react";
import { useMutation, useQuery } from "@apollo/client";

import { diff } from "deep-diff";
import gql from "graphql-tag";
import { useFilters } from "../../Filters/Filters.hook";
import { useQuerySkipAuth } from "../../../GlobalHooks/useQuerySkipAuth";

const sameSearch = (s1, s2) =>
	!diff(s1, s2, {
		normalize: (path, key, hls, hrs) => {
			if (key === "CurrencyID") {
				if (!s1.minPrice && !s1.maxPrice) hls = 1;
				if (!s2.minPrice && !s2.maxPrice) hrs = 1;
				return [hls, hrs];
			}

			if (["page", "order"].includes(key)) {
				return [1, 1];
			}

			if (
				typeof hls == "undefined" &&
				(hrs === null || (typeof hrs == "object" && hrs.length == 0))
			) {
				return [1, 1];
			}
			return false;
		},
	});

const FRAGMENT_DATA = `
    id
    ...on Individual {
        searchAlerts(status: 1) {
            id
            filters
            status
        }
    }
`;

const MUTATION_TOGGLE_SEARCH_ALERT = gql`
    mutation toggleSearchAlert($search: SearchParamsInput!, $frequency: Int!) {
        toggleSearchAlert(search: $search, frequency: $frequency) {
            ${FRAGMENT_DATA}
        }
    }
    
`;

const QUERY_SAVED_SEARCH_ALERTS_SMALL = gql`
    query savedAlerts {
        me {
           ${FRAGMENT_DATA}
        }
    }
    
`;

export const useCreateSearchAlert = () => {
	const { data, loading, error } = useQuerySkipAuth(QUERY_SAVED_SEARCH_ALERTS_SMALL);
	const { filters } = useFilters();
	const [mutation, response] = useMutation(MUTATION_TOGGLE_SEARCH_ALERT, {
		onError: () => {},
	});
	const [alertIsSaved, setAlertIsSaved] = useState(false);

	useEffect(() => {
		if (data) {
			setAlertIsSaved(
				data.me?.searchAlerts?.reduce(
					(acc, curr) => acc || sameSearch(curr.filters, filters),
					false
				)
			);
		} else {
			setAlertIsSaved(false);
		}
	}, [response, filters]);

	const createSearchAlert = () => {
		return mutation({
			variables: {
				frequency: 24, // es fijo
				search: filters,
			},
		});
	};

	return {
		searchAlert: { send: createSearchAlert, response: response },
		alertIsSaved,
		loading,
	};
};
