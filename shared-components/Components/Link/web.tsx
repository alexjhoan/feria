import { FC } from "react";
import { useRouter } from "next/router";
import { encodeHashUrl } from "../../Utils/Functions";

export interface LinkProps {
	pathname?: string;
	data?: any;
	as?: string;
	avoidRouter?: boolean;
	callback?: () => void | null;
	isExternal?: boolean;
}

export const PageLink: FC<LinkProps> = ({
	pathname,
	data,
	as,
	children,
	avoidRouter,
	callback = null,
	isExternal = false
}) => {
	const router = useRouter();

	const onClick = e => {
		if (e.ctrlKey || e.metaKey) return false;
		if (!avoidRouter && !isExternal) {
			e.preventDefault();
			if (typeof callback == "function") callback();
			router.push({ pathname: pathname, query: { hashed: encodeHashUrl(data) } }, "/" + as);
		}

		if(isExternal) {
			e.preventDefault();
			window.open(as, '_blank', 'location=yes,height=670,width=620,scrollbars=yes,status=yes')
		}
	};
	return (
		<>
			<a className="containerLink" onClick={onClick} href={isExternal ? "#" : "/" + as}>
				{children}
			</a>
			<style jsx>{`
				.containerLink {
					width: 100%;
					height: 100%;
				}
				.containerLink:hover {
					cursor: pointer;
				}
			`}</style>
		</>
	);
};
