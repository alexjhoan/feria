import { FC } from "react";
import theme from "../../Styles/_toBeDeleted_theme";

export interface LoaderProps {
	background?: string;
	color?: string;
	width?: string;
	height?: string;
}

export const Loader: FC<LoaderProps> = ({ background, color, width, height }) => {
	return (
		<>
			<div className="loader"></div>
			<style jsx>{`
				.loader {
					border: 3px solid ${typeof background != "undefined" ? background : "#f3f3f3"}; /* Light grey */
					border-top: 3px solid
						${typeof color != "undefined" ? color : theme.colors.primary};
					border-radius: 50%;
					width: ${typeof width != "undefined" ? width : "12px"};
					height: ${typeof height != "undefined" ? height : "12px"};
					animation: spin 1s linear infinite;
					margin: 0 auto;
				}

				@keyframes spin {
					0% {
						transform: rotate(0deg);
					}
					100% {
						transform: rotate(360deg);
					}
				}
			`}</style>
		</>
	);
};
