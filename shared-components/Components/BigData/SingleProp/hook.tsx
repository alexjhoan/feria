import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { useBigDataSearchResult } from "../SearchResult/hook";
import { makePriceText, parseMoney, formatMoney } from "../../../Utils/Functions";
import { useState, useEffect } from "react";

export const FRAGMENT_PROPERTY_BIGDATA = new FragmentDefiner(
	"Property",
	`
    price {
        amount
        currency {
					 name
        }
    }
    price_amount_usd
    m2
    created_at
    operation_type {
       name
    }
`
);

interface propData {
	price_text: String;
	price: Number;
	time_in_market: Number;
	price_m2: Number;
	price_m2_text: String;
}

interface offsetData {
	price: Number;
	time_in_market: Number;
	price_m2: Number;
}

export const useBigDataSingleProp = ({ property_id }) => {
	const { bigData } = useBigDataSearchResult({ searchFilters: { property_id } });
	const { data: property_data } = useReadFragment(FRAGMENT_PROPERTY_BIGDATA, property_id);

	let propData = null;
	let offsetData = null;

	if (property_data && bigData.data) {
		const currency = "U$S";
		const time_in_market =
			(Date.now() - new Date(property_data.created_at).getTime()) / (24 * 3600 * 1000);
		const price_m2 = Math.round(property_data.price_amount_usd / property_data.m2);

		propData = {
			price_text: "U$S " + formatMoney(property_data.price_amount_usd),
			price: property_data.price_amount_usd,
			time_in_market,
			price_m2,
			price_m2_text: `${currency} ${parseMoney(price_m2, 0, 1)}`,
		};

		offsetData = {
			price:
				Math.round((property_data.price_amount_usd / bigData.data.avg_price) * 100) - 100,
			time_in_market:
				Math.round((time_in_market / bigData.data.avg_time_in_market) * 100) - 100,
			price_m2:
				property_data.m2 != 0
					? Math.round(
							(property_data.price_amount_usd /
								property_data.m2 /
								bigData.data.avg_price_m2) *
								100
					  ) - 100
					: null,
		};
	}

	return {
		loading: bigData.loading,
		error: bigData.error,
		bigData: bigData ? bigData?.data : false,
		propData,
		offsetData,
		currencySymbol: property_data ? property_data.price.currency.name : null,
		operation: property_data ? property_data.operation_type?.name : null,
		show: property_data ? property_data.m2 > 0 && offsetData : false,
	};
};
