import { Button, Typography } from "antd";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { Dayjs } from "dayjs";
import React from "react";
import { useTheme } from "../../../../Styles/ThemeHook";

export const CustomHeader = ({
	month,
	prevMonth,
	nextMonth,
}: {
	month: Dayjs;
	prevMonth?: any;
	nextMonth?: any;
}) => {
	const { theme } = useTheme();
	return (
		<>
			<div className={"header"}>
				{prevMonth && (
					<Button
						shape="circle"
						size={"small"}
						className={"secondary left"}
						onClick={prevMonth}>
						<LeftOutlined />
					</Button>
				)}
				<Typography>{month.format("MMMM")}</Typography>
				{nextMonth && (
					<Button
						shape="circle"
						size={"small"}
						className={"secondary right"}
						onClick={nextMonth}>
						<RightOutlined />
					</Button>
				)}
			</div>
			<style jsx>{`
				.header {
					padding: ${theme.spacing.smSpacing}px;
				}
			`}</style>
		</>
	);
};
