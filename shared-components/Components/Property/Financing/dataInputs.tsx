import { InputNumber, Select as AntdSelect, Slider as AntdSlider, Input as AntdInput } from "antd";

export const Input = ({ value, onChange, currency }) => (
	<InputNumber
		value={value}
		min={0}
		formatter={value => `${currency} ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
		parser={value => value.replace(`${currency} `, "").replace(/(\$|U$S)\s?|(\.*)/g, "")}
		onChange={onChange}
		style={{ width: "100%" }}
	/>
);

export const InputWithPercentage = ({ value, max, percentage, onChange, currency }) => {
	const changePercentage = (newPercentage) => {
		if(typeof newPercentage === "number"){
			let newValue = Math.round((newPercentage*max)/100);
			onChange(newValue);
		}
	} 

	return (
		<AntdInput.Group compact>
			<InputNumber
				value={value}
				min={0}
				max={max}
				onChange={onChange}
				formatter={value => `${currency} ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
				parser={value => value.replace(`${currency} `, "").replace(/(\$|U|S)\s?|(\.*)/g, "")}
				style={{ width: "77%" }}
			/>
			<InputNumber
				onChange={changePercentage}
				value={percentage}
				min={0}
				max={100}
				formatter={value => `${value}%`}
				parser={value => value.replace('%', "")}
				style={{ width: "23%", textAlign: "right" }}
			/>
		</AntdInput.Group>
	)
};

export const Select = ({ options, value, onChange }) => {
	const { Option } = AntdSelect;

	return (
		<AntdSelect value={value} onChange={onChange} style={{ width: "100%" }}>
			{options.map((option, i) => (
				<Option key={"financing_input_" + i + "_" + option.value} value={option.value}>
					{option.text}
				</Option>
			))}
		</AntdSelect>
	);
};

export const Slider = ({ value, max, onChange }) => {
	return <AntdSlider style={{ marginTop: 10 }} value={value} max={max} onChange={onChange} />;
};
