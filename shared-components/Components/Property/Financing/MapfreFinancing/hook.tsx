import { formatMoney, postRequest } from "../../../../Utils/Functions"
import { useEffect, useState } from "react"
import { FragmentDefiner, useReadFragment } from "../../../../GlobalHooks/useReadFragment";

const MAPFRE_SERVICE = "https://prod.infocasas.com.uy/microservices/simulador/?/mapfre/simulation";
const FRAGMENT_PRICE = new FragmentDefiner('Property', `
  price {
    currency {
      name
    }
    amount
  }
`);

export const useMapfreFinancing = ({ property_id }) => {
  const { data } = useReadFragment(FRAGMENT_PRICE, property_id);
  const [loading, setLoading] = useState(true);
  const [priceAmount, setPriceAmount] = useState(0);
  const [currency, setCurrency] = useState(null);
  const [minimumIncome, setMinimumIncome] = useState(0);
  const [annualFees, setAnnualFees] = useState(0);
  const [cash, setCash] = useState(0);
  const [extraData, setExtraData] = useState(null);
  let timer = null;

  useEffect(() => {
    if(data){
      setPriceAmount(data.price.amount);
      setCurrency(data.price.currency.name);
    }
  }, [data]);

  useEffect(() => {
    const mapfreService = async () => {
      setLoading(true);
      let data = await postRequest(MAPFRE_SERVICE, {
        monto: priceAmount,
        IDpropiedad: property_id
      });

      if (data) {
        setMinimumIncome(data.ingresos_necesarios);
        setAnnualFees(data.costo_12_cuotas);
        setCash(data.costo_contado);
        setExtraData({
          importe_alquiler: formatMoney(priceAmount),
          ingresos_necesarios: `${currency} ${formatMoney(data.ingresos_necesarios)}`,
          costo_contado: `${currency} ${formatMoney(data.costo_contado)}`,
          costo_3_cuotas: `${currency} ${formatMoney(data.costo_3_cuotas)}`,
          costo_12_cuotas: `${currency} ${formatMoney(data.costo_12_cuotas)}`
        });
        setLoading(false);
      }
    }

    clearTimeout(timer);
    timer = setTimeout(() => {
      if(priceAmount > 0){
        mapfreService()
      }
    }, 600);

    return () => clearTimeout(timer);
  }, [priceAmount])

  return {
    loading,
    currency,
    priceAmount: {
      value: priceAmount,
      change: setPriceAmount
    },
    minimumIncome,
    annualFees,
    cash,
    extraData
  }
} 