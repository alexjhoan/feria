import { useSubsidiary } from "./hook";
import React, { MouseEvent, useState } from "react";
import { Button, Card, Col, Modal, Row, Space } from "antd";
import Text from "antd/lib/typography/Text";
import Paragraph from "antd/lib/typography/Paragraph";
import { useTranslation } from "react-i18next";
import { ModalPhone } from "../../ModalPhone/web";
import "./styles.less";
import { useTheme } from "../../../../Styles/ThemeHook";

export const Subsidiary = ({ subsidiary_id, property_id, mode }) => {
	const { subsidiary, loading, phone, viewPhone } = useSubsidiary({
		subsidiary_id,
	});

	const [modal, setModal] = useState(false);
	const { t } = useTranslation();
	const { theme } = useTheme();

	const handleClick = (e: MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		if (!phone) {
			viewPhone(property_id).then(data => {
				if (data) setModal(true);
			});
		} else {
			setModal(true);
		}
	};

	return (
		<>
			<Card key={subsidiary.id} size={"small"} className="subsidiary">
				<Row gutter={16} align={"middle"}>
					<Col flex={1} className="left">
						<Text strong className="title">
							{subsidiary.name}
						</Text>
						<Paragraph
							className="paragraph"
							ellipsis={{ rows: 1, expandable: true, symbol: t("Más") }}>
							{subsidiary.address}
						</Paragraph>
						<Text type={"secondary"} className={"hours"}>
							{subsidiary.office_hours}
						</Text>
					</Col>
					<Col>
						<Space size={4}>
							<Text className="masked">{subsidiary.masked_phone}</Text>
							<Button loading={loading} onClick={handleClick} className="secondary">
								{t("Ver teléfono")}
							</Button>
						</Space>
					</Col>
				</Row>

				<ModalPhone
					mode={mode}
					phone={phone}
					owner_name={subsidiary.name}
					property_id={property_id}
					loading={loading}
					visible={modal}
					onCancel={() => setModal(false)}
				/>
			</Card>
			<style jsx global>{`
				.subsidiary .ant-card-body {
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.mdSpacing}px;
				}
				.subsidiary .title {
					margin-bottom: ${theme.spacing.smSpacing}px;
				}
				.subsidiary .left .ant-typography,
				.subsidiary .ant-btn {
					font-size: ${theme.fontSizes.smFontSize};
					line-height: ${theme.fontSizes.lgFontSize};
				}
			`}</style>
		</>
	);
};
