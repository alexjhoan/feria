import React, { ReactElement } from "react";
import { Col, Row } from "antd";
import { PhoneButton } from "./PhoneButton/web";
import { WhatsappButton } from "./WhatsappButton/web";
import { useWhatsppButton } from "./WhatsappButton/hook";
import { FormButton } from "./FormButton/web";
import { useTheme } from "../../../../Styles/ThemeHook";
import { PropComponentMode } from "../../PropertyInterfaces";

export const LeadButtons = ({
	id,
	children = null,
	mode,
}: {
	id: any;
	children?: ReactElement;
	mode: PropComponentMode;
}) => {
	const { hasWhatsapp } = useWhatsppButton(id);
	const { theme } = useTheme();

	return (
		<>
			<Row gutter={theme.spacing.mdSpacing} className="property-lead-button">
				<Col flex={1}>{children ?? <FormButton id={id} />}</Col>
				<Col>
					<PhoneButton id={id} type={"callButton"} mode={mode} />
				</Col>
				{hasWhatsapp && (
					<Col>
						<WhatsappButton id={id} />
					</Col>
				)}
			</Row>
			<style jsx global>{`
				.property-card .property-lead-button {
					margin-top: ${theme.spacing.smSpacing}px;
				}
				.property-lead-button {
					flex-wrap: nowrap;
				}
			`}</style>
		</>
	);
};
