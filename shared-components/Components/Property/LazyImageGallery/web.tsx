import "./styles.less";
import "react-magic-slider-dots/dist/magic-dots.css";

import { Carousel, Spin } from "antd";
import { useMarkSeen, useSeenStatus } from "../SeenStatus/SeenStatus.hook";

import { CarouselArrow } from "./CarouselArrow/web";
import { CarouselProps } from "antd/lib/carousel";
import { ImagenOptimizada } from "../../Image/web";
import MagicSliderDots from "react-magic-slider-dots";
import React from "react";
import { useAuthCheck } from "../../User/useAuthCheck";
import { useLazyImageGallery } from "./LazyImageGallery.hook";
import { useTheme } from "../../../Styles/ThemeHook";

export const LazyImageGallery = ({ id, mode = "auto", staticSlide = false }) => {
	const { getImages, gallery, loading, error, called } = useLazyImageGallery({
		id,
		mode,
	});
	const { theme } = useTheme();
	const { markSeen } = useMarkSeen();
	const { result } = useSeenStatus({ id });
	const { isLoggedIn } = useAuthCheck();

	if (error) return null;

	const onHover = e => {
		e.preventDefault();
		if (!staticSlide && !called) getImages();
	};

	const onChangeImage = e => {
		if (isLoggedIn && !result && e != 0) markSeen(id);
	};

	const settings: CarouselProps = {
		dots: true,
		arrows: true,
		nextArrow: <CarouselArrow side={"right"} />,
		prevArrow: <CarouselArrow side={"left"} />,
		infinite: false,
		autoplay: false,
		slidesToScroll: 1,
		slidesToShow: 1,
		speed: 300,
		style: {
			height: "100%",
			width: "100%",
			objectFit: "cover",
		},
		appendDots: dots => {
			return <MagicSliderDots dots={dots} numDotsToShow={5} dotWidth={20} />;
		},
	};

	return (
		<>
			<div
				onMouseOver={e => onHover(e)}
				onTouchStart={e => onHover(e)}
				className={"lazy-loader"}>
				{/* {loading && (
					<div className={"gallery-image loader"}>
						<Spin />
					</div>
				)} */}
				<Carousel {...settings} afterChange={onChangeImage}>
					{gallery.map((g, i) => {
						return (
							<div className={"gallery-image"} key={"Gallery_" + i}>
								<ImagenOptimizada src={g.image} />
							</div>
						);
					})}
				</Carousel>
			</div>
			<style jsx global>{`
				.lazy-loader .gallery-image.loader .ant-spin-dot-item {
					background-color: ${theme.colors.secondaryColor};
				}
			`}</style>
		</>
	);
};
