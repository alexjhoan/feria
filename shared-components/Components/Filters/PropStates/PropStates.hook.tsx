import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";

import { showPropState } from "../../../Utils/Functions";
import { SelectTypes } from "../../Inputs/Selects/Selects.hook";
import { useFilters } from "../Filters.hook";

export const FRAGMENT_PROPSTATES_OPTIONS = new FragmentDefiner(
	"Filter",
	` id
	  name
	  options`
);
export const FRAGMENT_PROPSTATES = new FragmentDefiner(
	"Filters",
	`propStates { ${FRAGMENT_PROPSTATES_OPTIONS.query()} }`
);

export const usePropStates = props => {
	const { filters, changeFilters } = useFilters();
	const {
		labeled = false,
		selectedValue = filters?.constStatesID,
		inputType = "select",
		currentFilters = filters,
		filterChanged = changeFilters,
	} = props;
	const { loading, data, error } = useReadFragment(FRAGMENT_PROPSTATES_OPTIONS, "propStates");

	const handleChange = newValue => {
		let res = [];
		if (newValue) {
			res = newValue.map(v => {
				return { value: v["id"], text: v["name"] };
			});
		}
		filterChanged({ constStatesID: res });
	};

	const show = showPropState(currentFilters.property_type_id, currentFilters.operation_type_id);
	const type: SelectTypes = "multiple";

	return {
		loading: loading,
		error: error,
		show,
		data: {
			options: data?.options,
			onChange: handleChange,
			type: type,
			value: selectedValue,
			maxCountOptions: 3,
		},
		label: data?.name,
		labeled,
	};
};
