import { Label } from "../../Inputs/Label/app";
import { CheckBox } from "../../Inputs/app";
import { usePrivateOwner } from "./PrivateOwner.hook";
import { FilterComponentType } from "../Filters.hook";
import { Text, View } from "react-native";
import React from "react";

export const PrivateOwner = (props: FilterComponentType) => {
	const { loading, data, error, show } = usePrivateOwner(props);

	if (!show) return null;
	if (loading) return null;
	if (error) return <Text>error</Text>;

	return (
		<>
			<View>
				<CheckBox {...data} />
			</View>
		</>
	);
};
