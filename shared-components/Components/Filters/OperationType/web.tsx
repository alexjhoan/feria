import "./styles.less";

import { Col, Collapse, Radio, Row, Select, Skeleton, Space, Tooltip, Typography } from "antd";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { useOperationType } from "./OperationType.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Option } = Select;
const { Panel } = Collapse;

export const OperationType = ({ collapsable = false, home = false, ...props }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		show,
		error,
		loading,
		inputType,
		label,
		labeled,
		data: { options, value, onChange, customKey },
	} = useOperationType(props);

	if (!show) return null;
	if (error) return null;

	const opts = options ? options.filter(o => (home ? o.show_in_home : true)) : [];

	const handleChange = e => {
		let val = e;
		const res = options.filter(o => o[customKey] == val);
		onChange(res[0]);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filterRadio = loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Radio.Group onChange={e => handleChange(e.target.value)} value={value} disabled={loading}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{opts.map(o => (
					<Col span={24} key={`key_${o[customKey]}_operationType`}>
						<Radio value={o[customKey]}>{t(o.name)}</Radio>
					</Col>
				))}
			</Row>
		</Radio.Group>
	);

	const filterButtons = loading ? (
		<Space style={{ width: "100%", justifyContent: "center" }} size={theme.spacing.xsSpacing}>
			<Skeleton.Button active shape="round" style={{ width: 170 }} />
			<Skeleton.Button active shape="round" style={{ width: 170 }} />
			<Skeleton.Button active shape="round" style={{ width: 170 }} />
		</Space>
	) : (
		<>
			<Radio.Group
				disabled={loading}
				onChange={e => handleChange(e.target.value)}
				optionType="button"
				buttonStyle={"solid"}
				value={value}>
				{opts.map(o => (
					<Radio.Button
						key={`key_buttons_${o[customKey]}_operationType`}
						value={o[customKey]}>
						{t(o.name)}
					</Radio.Button>
				))}
			</Radio.Group>
			<style jsx global>{`
				.home-filters .operation-type-filter .operation-type-skeleton .ant-skeleton-button {
					border-radius: ${theme.spacing.smSpacing}px;
				}

				.home-filters .operation-type-filter .ant-radio-group {
					background: ${theme.colors.backgroundModalColor};
				}

				.home-filters
					.operation-type-filter
					.ant-radio-group:hover
					.ant-radio-button-wrapper-checked:not(:hover) {
					color: ${theme.colors.backgroundColor};
				}

				.home-filters .operation-type-filter .ant-radio-group,
				.home-filters .operation-type-filter .ant-radio-group .ant-radio-button-wrapper {
					border-radius: ${theme.spacing.smSpacing}px;
				}

				.home-filters
					.operation-type-filter
					.ant-radio-group
					.ant-radio-button-wrapper:not(.ant-radio-button-wrapper-checked) {
					color: ${theme.colors.backgroundColor};
				}

				.home-filters
					.operation-type-filter
					.ant-radio-group
					.ant-radio-button-wrapper:not(:first-child)::before {
					background: ${theme.colors.borderColor};
				}

				.home-filters
					.operation-type-filter
					.ant-radio-group
					.ant-radio-button-wrapper-checked,
				.home-filters
					.operation-type-filter
					.ant-radio-group
					.ant-radio-button-wrapper-checked:hover,
				.home-filters
					.operation-type-filter
					.ant-radio-group
					.ant-radio-button-wrapper:hover {
					background: ${theme.colors.backgroundColor};
					border-color: ${theme.colors.backgroundColor};
					color: ${theme.colors.secondaryColor};
					font-weight: bold;
				}
			`}</style>
		</>
	);

	const filterSelect = loading ? (
		<Skeleton.Button active className="operation-type-skeleton" />
	) : (
		<>
			<Select
				disabled={loading}
				value={loading ? t("Operación") : value}
				style={{ width: "100%" }}
				onChange={handleChange}>
				{opts.map(o => (
					<Option key={`key_select_${o[customKey]}_operationType`} value={o[customKey]}>
						{t(o.name)}
					</Option>
				))}
			</Select>
			<style jsx global>{`
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.home-filters .operation-type-filter .ant-select .ant-select-selector {
						border-radius: ${theme.spacing.smSpacing}px;
					}
				}
			`}</style>
		</>
	);

	let filter = filterButtons;
	if (inputType == "radioselect") filter = filterRadio;
	else if (inputType == "select") filter = filterSelect;

	return (
		<div className="filter operation-type-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={value ? "false" : "true"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={value && extraIcon()}
						disabled={loading}
						header={t("Tipo de Operación")}
						key="true">
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title level={4} disabled={loading}>
							{label}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
