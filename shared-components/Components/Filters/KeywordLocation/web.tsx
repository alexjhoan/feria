import "./styles.less";

import { AutoComplete, Col, Input, Row, Skeleton, Space, Typography } from "antd";
import { CloseOutlined, SearchOutlined } from "@ant-design/icons";
import { KeywordLocationProps, useKeywordLocation } from "./KeywordLocation.hook";
import React, { useEffect, useRef, useState } from "react";

import { LoadingOutlined, KeyOutlined } from "@ant-design/icons";
import { LocationIcon } from "../../CustomIcons/web";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { useRouter } from "next/router";
import { encodeHashUrl } from "../../../Utils/Functions";

export const KeywordLocation = (props: KeywordLocationProps) => {
	const { theme } = useTheme();
	const router = useRouter();

	const { t } = useTranslation();
	const {
		show,
		keyword,
		onSearch,
		onSelect,
		searchResults,
		refResults,
		searchLoading,
		location,
	} = useKeywordLocation(props);
	const ref = useRef(null);

	useEffect(() => {
		if (location) ref.current.blur();
	}, [location]);

	const [isOpen, setOpen] = useState(false);

	if (!show) return null;

	const renderOption = ({ value, label, data, group = null, icon }) => {
		return {
			data: data,
			value: value,
			label: (
				<Row>
					<Col style={{ marginRight: theme.spacing.smSpacing }}>{icon}</Col>
					<Typography.Text ellipsis style={{ flex: "1" }}>
						{label}
					</Typography.Text>
					{group && (
						<Col style={{ marginLeft: theme.spacing.smSpacing }}>
							<Typography.Text type="secondary">{group}</Typography.Text>
						</Col>
					)}
				</Row>
			),
		};
	};
	const renderTitle = title => {
		return (
			<Row>
				<Typography.Text strong>{title}</Typography.Text>
			</Row>
		);
	};

	let options = [];
	let optionsData = [];
	let optionsTitle = "";

	// location options && recient searches
	if (searchResults?.searchLocation.length > 0 && keyword.length > 0) {
		optionsData = searchResults.searchLocation;
		optionsTitle = t("Ubicacion");
	} else if (keyword.length == 0 && isOpen) {
		optionsData = getRecientLocations();
		optionsTitle = t("Recientes");
	}
	if (optionsData.length > 0) {
		options = [
			{
				label: renderTitle(optionsTitle),
				options: optionsData.map(o => {
					const label =
						o.__typename == "Neighborhood" ? o.name + ", " + o.estate.name : o.name;
					const group = o.__typename == "Neighborhood" ? "Barrio" : "Departamento";
					const icon = <LocationIcon style={{ fontSize: theme.fontSizes.smTitle }} />;
					return renderOption({
						value: label,
						label,
						data: { ...o, label: label },
						icon,
						group,
					});
				}),
			},
		];
	}

	// reference options
	if (refResults?.searchByRef) {
		options = [
			...options,
			{
				label: renderTitle(t("Código de Referencia")),
				options: [
					renderOption({
						label: (
							<>
								<Typography.Text>{refResults.searchByRef.title}</Typography.Text>
							</>
						),
						value: refResults.searchByRef.id,
						data: refResults.searchByRef,
						icon: <KeyOutlined />,
					}),
				],
			},
		];
	}

	// keyword option
	if (!searchLoading && keyword.length > 0) {
		options = [
			...options,
			{
				label: renderTitle(t("Palabra Clave")),
				options: [
					renderOption({
						label: (
							<>
								{t("Buscar inmuebles relacionados a ")}
								<Typography.Text strong>{keyword}</Typography.Text>
							</>
						),
						value: " " + keyword,
						data: { label: keyword, __typename: "keyword" },
						icon: <SearchOutlined />,
					}),
				],
			},
		];
	}

	return (
		<>
			{searchLoading && keyword.length === 0 ? (
				<Skeleton.Input active className={"keyword-location-skeleton"} />
			) : (
				<AutoComplete
					className={"filter keyword-location " + props.className}
					ref={ref}
					backfill
					allowClear
					clearIcon={<CloseOutlined />}
					value={keyword}
					options={options}
					notFoundContent={
						<Space style={{ padding: `${theme.spacing.xsSpacing}px 0` }}>
							{keyword != "" && <LoadingOutlined style={{ fontSize: 18 }} />}
							<Typography.Text type={"secondary"}>
								{keyword != ""
									? t("Cargando")
									: t("Escribe una ubicación o palabra clave")}
							</Typography.Text>
						</Space>
					}
					dropdownClassName={"dropdown-keyword-location"}
					onSearch={onSearch}
					onSelect={(a, b) => {
						if (b.data.__typename == "Property") {
							if (!b.data.project[0]?.isEspecial && !b.data.isExternal)
								router.push(
									{
										pathname: b.data.project[0]
											? "/projectSingle"
											: "/propSingle",
										query: { hashed: encodeHashUrl(b.data) },
									},
									"/" + b.data.link
								);

							if (b.data.isExternal)
								window.open(
									b.data.link,
									"_blank",
									"location=yes,height=670,width=620,scrollbars=yes,status=yes"
								);
						} else {
							onSelect(a, b.data);
						}
					}}
					onFocus={() => {
						onSearch("");
					}}
					onBlur={e => {
						if (location) onSearch(location.label);
					}}
					onDropdownVisibleChange={e => setOpen(e)}
					placeholder={t("Buscá por ubicación o palabra clave")}>
					<Input
						onPressEnter={() => {
							if (keyword)
								onSelect(keyword, { label: keyword, __typename: "keyword" });
						}}
					/>
				</AutoComplete>
			)}
			<style jsx global>{`
				.dropdown-keyword-location .ant-select-item-option-grouped,
				.dropdown-keyword-location .ant-select-item {
					padding: ${theme.spacing.mdSpacing / 2}px ${theme.spacing.smSpacing}px;
				}

				.dropdown-keyword-location .ant-select-item.ant-select-item-group {
					padding: ${theme.spacing.xsSpacing}px ${theme.spacing.smSpacing}px;
					background: ${theme.colors.backgroundColorAternative};
				}

				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.home-filters .keyword-location .ant-input {
						border-radius: ${theme.spacing.smSpacing}px 0 0 ${theme.spacing.smSpacing}px;
						position: relative;
						-webkit-appearance: none;
					}
				}
			`}</style>
		</>
	);
};

export const LOCAL_STORAGE_KEY = "RecientLocations";
const getRecientLocations = () => {
	const items = localStorage.getItem(LOCAL_STORAGE_KEY);
	let res = [];
	if (items) res = JSON.parse(items);
	return res;
};
export const setRecientLocations = (neighborhood, estate) => {
	if (estate) {
		let location = {};
		location["id"] = estate["value"];
		location["name"] = estate["text"];
		location["__typename"] = "Estate";
		if (neighborhood) {
			location["estate"] = { ...location };
			location["id"] = neighborhood[0]["value"];
			location["name"] = neighborhood[0]["text"];
			location["__typename"] = "Neighborhood";
		}
		let res = getRecientLocations();
		res = res.filter(l => l.id != location["id"] || l["__typename"] != location["__typename"]);
		res = [{ ...location }, ...res];
		if (res.length > 5) res.pop();
		localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(res));
	}
};
