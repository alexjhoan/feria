import { ButtonSelect } from "../../Inputs/app";
import { useBedrooms } from "./Bedrooms.hook";

import { Label } from "../../Inputs/Label/app";
import React from "react";
import { View, Text } from "react-native";
import { FilterComponentType } from "../Filters.hook";

const Bedrooms = (props: FilterComponentType) => {
	const { loading, data, error, show, label, labeled } = useBedrooms(props);

	if (!show) return null;
	if (loading) return <Text>loadingxxx</Text>;
	if (error) return <Text>error</Text>;

	return (
		<View>
			<Label labelContent={label} showLabel={labeled}>
				<ButtonSelect {...data} />
			</Label>
		</View>
	);
};

export { Bedrooms };
