import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { showBedrooms } from "../../../Utils/Functions";
import { SelectTypes } from "../../Inputs/Selects/Selects.hook";
import { useFilters } from "../Filters.hook";

const FRAGMENT_BEDROOMS_OPTIONS = new FragmentDefiner(
	"Filter",
	`
		  id
		  name
		  options
	  `
);

export const FRAGMENT_BEDROOMS = new FragmentDefiner(
	"Filters",
	`
		  bedrooms {
			  ${FRAGMENT_BEDROOMS_OPTIONS.query()}
		  }
	  `
);

export const useBedrooms = props => {
	const { filters, changeFilters } = useFilters();
	const {
		labeled = false,
		selectedValue = filters?.bedrooms,
		inputType = "select",
		currentFilters = filters,
		filterChanged = changeFilters,
	} = props;
	const { loading, data, error } = useReadFragment(FRAGMENT_BEDROOMS_OPTIONS, "bedrooms");

	let options = [];
	if (data) {
		options = data.options.map(element => {
			let cant = element.amount;
			return {
				value: element.amount,
				amount: (cant + (cant >= data.options.length - 1 ? "+" : "")).replace("0", "M"),
			};
		});
	}

	const handleChange = newValue => {
		let res = [];
		if (newValue != null) {
			res = newValue.map(v => {
				return {
					value: v["value"],
					text:
						v["amount"] == "M"
							? `Monoambiente`
							: v["amount"] == 1
							? `${v["amount"]} Dormitorio`
							: `${v["amount"]} Dormitorios`,
				};
			});
		}
		filterChanged({ bedrooms: res });
	};

	const show = showBedrooms(currentFilters.property_type_id, currentFilters.operation_type_id);
	const type: SelectTypes = "multiple";

	return {
		loading: loading,
		error: error,
		show,
		data: {
			onChange: handleChange,
			options: options,
			value: selectedValue,
			customKey: "value",
			valueKey: "amount",
			type: type,
		},
		label: data?.name,
		labeled,
	};
};
