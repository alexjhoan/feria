import {usePropertySimilars} from "../hook";

export const usePropertySimilarsMap = ({property_id}) => {
    return usePropertySimilars({property_id});
}