import "./styles.less";

import { PageHeader, Space, Typography } from "antd";
import React, { useEffect, useState } from "react";

import { ArrowLeftOutlined } from "@ant-design/icons";
import { BreadCrumbs } from "../../../Components/SEO/web";
import { useRouter } from "next/router";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Text } = Typography;

export const Navigation = ({ type = "default" }: { type: "default" | "small" | "none" }) => {
	const router = useRouter();
	const { t } = useTranslation();
	const { theme } = useTheme();

	const [backButton, setbackButton] = useState(false);
	useEffect(() => setbackButton(typeof history !== "undefined" && history.length > 1), []);

	return (
		<>
			<PageHeader
				className="pd-navigation"
				backIcon={
					backButton && (
						<Space size={theme.spacing.smSpacing}>
							<ArrowLeftOutlined />
							{type == "none" ? null : type !== "small" ? (
								<Text>{t("Volver a la búsqueda")}</Text>
							) : (
								<Text>{t("Volver")}</Text>
							)}
						</Space>
					)
				}
				onBack={() => router.back()}
				title={type !== "default" && " "}
				subTitle={type == "default" && <BreadCrumbs />}
			/>
			<style jsx global>{`
				.ant-page-header-back-button * {
					color: ${theme.colors.secondaryColor};
					font-size: ${theme.fontSizes.smFontSize};
					font-weight: bold;
				}

				.header .ant-page-header-back-button * {
					color: ${theme.colors.primaryColor};
					font-size: ${theme.fontSizes.lgFontSize};
				}
				.header .ant-page-header-back-button:hover * {
					color: ${theme.colors.primaryDarkColor};
				}
				.ant-page-header-back-button:hover * {
					color: ${theme.colors.secondaryHoverColor};
				}
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.ant-page-header-back {
						margin-right: ${theme.spacing.smSpacing}px;
					}
				}
			`}</style>
		</>
	);
};
