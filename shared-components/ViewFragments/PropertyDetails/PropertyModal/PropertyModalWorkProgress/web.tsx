import React, { useState } from "react";
import { Carousel, Space, Typography } from "antd";
import { CarouselArrow } from "../../../../Components/Property/LazyImageGallery/CarouselArrow/web";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useWorkProgress } from "../../../../Components/Project/WorkProgress/WorkProgress.hook";
import { PropComponentMode } from "../../../../Components/Property/PropertyInterfaces";
import { ImagenOptimizada } from "../../../../Components/Image/web";
import "./styles.less";

const { Text } = Typography;
export const PropertyModalWorkProgress = ({
	id,
	mode = "auto",
}: {
	id: string;
	mode: PropComponentMode;
}) => {
  const { loading, images, error } = useWorkProgress(id);
	const { theme } = useTheme();
	const [current, setCurrent] = useState(0);
	if (error) return null;
	if (loading) return <>...</>;

	const settings = {
		dots: false,
		arrows: true,

		nextArrow: <CarouselArrow side={"right"} />,
		prevArrow: <CarouselArrow side={"left"} />,
		infinite: true,
		autoplay: false,
		slidesToScroll: 1,
		slidesToShow: 1,
		speed: 300,
		style: {
			height: "100%",
		},
	};

	return (
		<>
			<div className={"property-modal-workprogress"}>
				<Carousel {...settings} afterChange={setCurrent}>
					{images.map((g, i) => {
            console.log("imagen es", g.image)
						return (
							<div className={"gallery-image"} key={"Gallery_" + i}>
								<ImagenOptimizada src={g.image} />
							</div>
						);
					})}
				</Carousel>
				<div className="photos-footer">
					<Space size={theme.spacing.xsSpacing}>
						<Text>{current+1}</Text>
						<Text>/</Text>
						<Text>{images.length}</Text>
					</Space>
				</div>
			</div>
			<style jsx>{`
				.property-modal-workprogress .photos-footer {
					border-color: ${theme.colors.textColor};
					padding: 0 ${theme.spacing.lgSpacing}px;
				}
				.property-modal-workprogress .photos-footer :global(.ant-typography) {
					color: ${theme.colors.textInverseColor};
					font-size: ${theme.fontSizes.smFontSize};
				}
			`}</style>
		</>
	);
};
