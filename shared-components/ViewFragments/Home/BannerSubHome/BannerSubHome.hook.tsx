import { gql } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { useFilters } from "../../../Components/Filters/Filters.hook";

const DESTACADO_SUBHOME_QUERY = gql`
	query SubBanners($limit: Int, $operationType: Int) {
		featuredSubBannersHome(limit: $limit, operationType: $operationType) {
			id
			url
			image
			name
			operation_type_id
		}
	}
`;

const DEFAULT_PROPS = {
	fetchPolicy: "cache-first",
	limit: 3,
};

const useBannerSubHome = props => {
	props = { ...DEFAULT_PROPS, ...props };

	const {
		filters: { operation_type_id },
	} = useFilters();

	const { data, loading, error } = useQuery(DESTACADO_SUBHOME_QUERY, {
		variables: {
			limit: props.limit,
			operationType: operation_type_id,
		},
		fetchPolicy: props.fetchPolicy,
	});

	return {
		loading: loading,
		data: data?.featuredSubBannersHome,
		error: error,
	};
};

export { useBannerSubHome };
