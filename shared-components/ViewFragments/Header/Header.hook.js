import React, { useState, createContext } from "react";
import { useQuery, useMutation, gql } from "@apollo/client";
import { MUTATION_LOCAL_STORE } from "../ApolloResolvers";
import CountryResolver, { QUERY_COUNTRY } from "../ApolloResolvers/Country";
import {useQuerySkipAuth} from "../../GlobalHooks/useQuerySkipAuth";

const CURRENT_USER_QUERY = gql`
  query ME_HEADER {
    me {
      firstName
      name
      id
      email
      individual
      logo
      country_id
      property_count
      notifications(first: 5) {
        data {
          id
          created_at
          text
          url
          new_tab
          seen
          image
        }
      }
      unread_notifications
    }
  }
`;
const HEADER_QUERY = gql`
  query MiConfiguration_Header {
    configuration {
      header_configuration {
        buttons {
          id
          text
          url
          order
          title
          image
          display
          orderMobile
          userShow
          classname
          children {
            id
            text
            url
            order
            title
            image
            classname
            display
            orderMobile
            userShow
          }
        }
      }
    }
  }
`;

const useCountrySelection = () => {
  const { data, loading, error } = useQuerySkipAuth(QUERY_COUNTRIES_LIST);
  const [country, setCountry] = useState({});
  const [MUTATOR_LOCAL_STORE] = useMutation(MUTATION_LOCAL_STORE);

  const save = (c) => {
    return MUTATOR_LOCAL_STORE({
      variables: {
        new_state: { ...c },
        query: QUERY_COUNTRY,
      },
    });
  };

  const remove = () => {
    save({ id: null });
  };

  return {
    countryList: { data: data, loading: loading, error: error },
    selected: country,
    select: setCountry,
    save: save,
    remove: remove,
  };
};

export { useCountrySelection };
