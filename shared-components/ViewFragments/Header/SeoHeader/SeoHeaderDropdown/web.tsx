import "./styles.less";

import { Card, Col, Menu, Row, Typography } from "antd";
import React, { useState } from "react";

import { SeoHeaderBanner } from "../SeoHeaderBanner/web";
import { useTheme } from "../../../../Styles/ThemeHook";

const { Text, Link } = Typography;

export const SeoHeaderDropdown = ({ links, banner }) => {
	const { theme } = useTheme();
	const [selectedItem, setSelectedItem] = useState("0");
	const handleOpenChange = (openItems: any[]) => {
		if (openItems.length) setSelectedItem(openItems[openItems.length - 1]);
	};

	return (
		<>
			<Row className="menu-dropdown">
				<Col className="sub-menu-dropdown-right">
					<Menu
						className="menu"
						openKeys={[selectedItem]}
						onOpenChange={handleOpenChange}
						theme={"dark"}
						triggerSubMenuAction={"hover"}>
						{links.map((e, i) => (
							<Menu.SubMenu key={i} title={e.title} />
						))}
					</Menu>
				</Col>

				{links.map((seoLink, i) => {
					if (selectedItem != i) return null;
					return (
						<Col key={"link_seoheader_" + i} className="sub-menu-dropdown-left">
							<Card bordered={false} title={seoLink.title}>
								{seoLink.categories.map((cateogry, j) => (
									<Card.Grid
										key={"link_seoheader_" + i + "_" + j}
										hoverable={false}>
										<Text>{cateogry.title}</Text>
										{cateogry.links.map((link, k) => (
											<div key={"link_seoheader_" + i + "_" + j + "_" + k}>
												<Link href={link.link}>{link.title}</Link>
											</div>
										))}
									</Card.Grid>
								))}
							</Card>
							{banner.img && <SeoHeaderBanner banner={banner} />}
						</Col>
					);
				})}
			</Row>
			<style jsx global>{`
				.menu-dropdown-popup .menu-dropdown .sub-menu-dropdown-right .menu {
					padding: ${theme.spacing.lgSpacing}px 0;
					font-size: ${theme.fontSizes.smFontSize};
				}

				.menu-dropdown-popup
					.menu-dropdown
					.sub-menu-dropdown-right
					.menu
					.ant-menu-submenu-active {
					background: ${theme.colors.primaryColor};
					color: ${theme.colors.backgroundColor};
				}

				.menu-dropdown-popup .menu-dropdow .sub-menu-dropdown-left .ant-card {
					padding: ${theme.spacing.lgSpacing}px;
				}

				.menu-dropdown-popup
					.menu-dropdow
					.sub-menu-dropdown-left
					.ant-card
					.ant-card-body
					.ant-card-grid {
					padding: ${theme.spacing.lgSpacing}px;
				}

				.menu-dropdown-popup
					.menu-dropdow
					.sub-menu-dropdown-left
					.ant-card
					.ant-card-body
					.ant-card-grid
					span {
					font-size: ${theme.fontSizes.lgFontSize};
				}

				.menu-dropdown-popup
					.menu-dropdow
					.sub-menu-dropdown-left
					.ant-card
					.ant-card-body
					.ant-card-grid
					a {
					font-size: ${theme.fontSizes.smFontSize};
					color: ${theme.colors.textInverseColor};
				}

				.menu-dropdown-popup
					.menu-dropdow
					.sub-menu-dropdown-left
					.banner_seoheader
					.ant-tag-processing {
					width: calc(100% - ${theme.spacing.xxlSpacing});
					font-size: ${theme.fontSizes.xsFontSize};
					line-height: ${theme.fontSizes.lgFontSize};
					color: ${theme.colors.backgroundColor};
					border-color: ${theme.colors.backgroundColor};
				}
			`}</style>
		</>
	);
};
