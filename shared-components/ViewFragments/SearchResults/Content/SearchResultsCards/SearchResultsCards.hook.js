import { gql } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { FRAGMENT_PROPERTY_CARD } from "../../../../Components/Property/PropertyCard/PropertyCard.hook";
import { useFilters } from "../../../../Components/Filters/Filters.hook";

const RESULTS_PER_PAGE = 20;
const QUERY_SEARCH_RESULTS_CARDS = gql`
    query ResultsGird($rows: Int!, $params: SearchParamsInput!, $page: Int) {
        properties: searchListing(params: $params, first: $rows, page: $page) {
            data {
               __typename
               id
                ${FRAGMENT_PROPERTY_CARD.query()}
            }
             paginatorInfo {
                lastPage
                firstItem
                lastItem
                total
             }
        }
    }
`;

const useSearchResultsCards = () => {
	const { filters } = useFilters();

	const { data, loading, error } = useQuery(QUERY_SEARCH_RESULTS_CARDS, {
		variables: {
			rows: RESULTS_PER_PAGE,
			params: filters,
			page: filters?.page,
		},
		skip: !(filters && filters.operation_type_id && filters.page) || (filters && filters.map_bounds && filters.map_bounds.length > 0),
	});

	return {
		loading,
		data: data?.properties.data,
		paginatorInfo: data?.properties.paginatorInfo,
		error,
	};
};

export { useSearchResultsCards };
