import { useSearchResultsPagination } from "../../Content/SearchResultsCards/SearchResultsPagination/SearchResultsPagination.hook";

export const useCloseSiderButton = () => {
	const {
		error,
		data: { total },
		loading,
	} = useSearchResultsPagination();

	return {
		error,
		data: total,
		loading,
	};
};
