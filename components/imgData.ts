export const LargeImg = [
  {
    image: '/images/invLarge/viasono.jpg',
    url: 'https://www.viasono.com.uy/catalogo?grp=18',
    whatsapp: 'https://wa.me/+59895780047',
    tinyAd: [
      {
        image: '/images/invLarge/tinyImg/viasono01.png', 
        url: 'https://www.viasono.com.uy/catalogo/sofa-eliot-la-paz-pinhao-3c-246cm_204050107_204050107'
      },
      {
        image: '/images/invLarge/tinyImg/viasono02.png', 
        url: 'https://www.viasono.com.uy/catalogo/alfombra-finlay_301010090_301010090'
      },
      {
        image: '/images/invLarge/tinyImg/viasono03.png', 
        url: 'https://www.viasono.com.uy/catalogo/colchon-resorte-galicia-flex-queen_126_42230'
      },
      {
        image: '/images/invLarge/tinyImg/viasono04.png', 
        url: 'https://www.instagram.com/https://www.viasono.com.uy/catalogo/colchon-resorte-serenity-viasono-queen_11_43893/'
      },
      {
        image: '/images/invLarge/tinyImg/viasono05.png', 
        url: 'https://www.viasono.com.uy/catalogo/mesa-de-comedor-skagen-roble-220x95cm_205010045_205010045'
      },
      {
        image: '/images/invLarge/tinyImg/viasono06.png', 
        url: 'https://www.viasono.com.uy/catalogo/poltrona-spike-gris-zuiver_203010054_203010054'
      }
    ]  
  },
  {
    image: '/images/invLarge/sackit.jpg',
    url: 'https://www.instagram.com/sackit_uruguay/',
    whatsapp: 'https://wa.me/+59897429119',
    tinyAd: [
      {
        image: '/images/invLarge/tinyImg/sackit01.png', 
        url: 'https://www.instagram.com/sackit_uruguay/'
      },
      {
        image: '/images/invLarge/tinyImg/sackit02.png', 
        url: 'https://www.instagram.com/sackit_uruguay/'
      },
      {
        image: '/images/invLarge/tinyImg/sackit03.png', 
        url: 'https://www.instagram.com/sackit_uruguay/'
      }
    ]   
  },
  {
    image: '/images/invLarge/tecnohaus.jpg',
    url: 'https://www.tecnohaus.com.uy/',
    whatsapp: 'https://wa.me/+59899921595'
  },
  {
    image: '',
    slides: [
      {
        img: '/images/invLarge/piscinas1.png',
        url: 'https://www.jetpool.uy/jProductosDatos.php?id_producto=37%20&id_producto_linea=0',
      },
      {
        img: '/images/invLarge/piscinas2.png',
        url: 'https://www.jetpool.uy/jProductosDatos.php?id_producto=41%20&id_producto_linea=0',
      }
    ],
    url: 'https://aguasypiscinas.uy/',  
    whatsapp: 'https://wa.me/+59894738484',
    tinyAd: [
      {
        image: '/images/invLarge/tinyImg/piscinas3.png', 
        url: 'https://www.jetpool.uy/jProductosDatos.php?id_producto=34%20&id_producto_linea=0'
      },
      {
        image: '/images/invLarge/tinyImg/piscinas4.png', 
        url: 'https://www.jetpool.uy/jProductosDatos.php?id_producto=115%20&id_producto_linea=0'
      },
      {
        image: '/images/invLarge/tinyImg/piscinas5.png', 
        url: 'https://www.jetpool.uy/jProductosDatos.php?id_producto=99%20&id_producto_linea=0'
      }
    ]
  },
  {
    image: '/images/invLarge/dante.jpg',
    url: 'https://empresadante.com.uy/',
    whatsapp: 'https://wa.me/+59898518120'
  },
  {
    image: '/images/invLarge/raiona.jpg',
    url: 'https://raionaboxes.com/',
    whatsapp: 'https://wa.me/+59898518120',
    tinyAd: [
      {
        image: '/images/invLarge/tinyImg/raiona01.jpg', 
        url: 'https://raionaboxes.com/con-soportes'
      },
      {
        image: '/images/invLarge/tinyImg/raiona02.jpg', 
        url: 'https://raionaboxes.com/rack-de-techo'
      },
      {
        image: '/images/invLarge/tinyImg/raiona03.jpg', 
        url: 'https://raionaboxes.com/colgante'
      }
    ]   
  },
  {
    image: '',
    slides: [
      {
        img: '/images/invLarge/fiji1.png',
        url: 'https://fijipiscinas.com/producto/monet/',
      },
      {
        img: '/images/invLarge/fiji2.png',
        url: 'https://fijipiscinas.com/producto/picasso/',
      },
      {
        img: '/images/invLarge/fiji3.png',
        url: 'https://fijipiscinas.com/producto/piscina-texas/',
      }
    ],
    url: 'https://fijipiscinas.com/categoria-producto/modelos/?gclid=CjwKCAjwn6GGBhADEiwAruUcKi_nPQ_3Y9unYxv8qOe7JEKvFlo9SPk3Lmp-3azcOMxdIk-bVhV5DxoCrNEQAvD_BwE',
    whatsapp: 'https://wa.me/+59899232777',
    tinyAd: [
      {
        image: '/images/invLarge/tinyImg/fiji4.png', 
        url: 'https://fijipiscinas.com/producto/dolphin-s100/'
      },
      {
        image: '/images/invLarge/tinyImg/fiji5.png', 
        url: 'https://fijipiscinas.com/producto/bomba-de-calor-mh3/'
      },
      {
        image: '/images/invLarge/tinyImg/fiji6.png', 
        url: 'https://fijipiscinas.com'
      }
    ]   
  },
  {
    image: '/images/invLarge/bertoni.jpg',
    url: 'https://tienda.bertoni.com.uy',
    whatsapp: 'https://wa.me/+59894141145',
    tinyAd: [
      {
        image: '/images/invLarge/tinyImg/bertoni2.jpg', 
        url: 'https://tienda.bertoni.com.uy/producto/mesa-de-comedor-tb-08/'
      },
      {
        image: '/images/invLarge/tinyImg/bertoni3.jpg', 
        url: 'https://tienda.bertoni.com.uy/producto/silla-comedor-pw-018/'
      },
      {
        image: '/images/invLarge/tinyImg/bertoni4.png', 
        url: 'https://tienda.bertoni.com.uy/producto/silla-mesh-new-en-color-negro/'
      }
    ]   
  },
  {
    image: '/images/invLarge/ashley.png',
    url: 'https://store.ashleyfurniture.uy/?utm_source=expo_hogar&utm_medium=banner&utm_campaign=expo_hogar_2021',
    whatsapp: 'https://wa.me/+59896207790'
  },
  {
    image: '/images/invLarge/hogartecno.png',
    url: 'https://tienda.hogartecno.com',
    whatsapp: 'https://wa.me/+59897127449'
  },
  {
    image: '/images/invLarge/decohogar.jpg',
    url: 'https://bit.ly/36sNvWi',
    whatsapp: 'https://wa.me/+59899535718',
    tinyAd: [
      {
        image: '/images/invLarge/tinyImg/decohogar2.png',
        url: 'https://www.decohogar.com.uy/decoracion-y-mobiliario/decoracion'
      },
      {
        image: '/images/invLarge/tinyImg/decohogar3.png', 
        url: 'https://www.decohogar.com.uy/decoracion-y-mobiliario/mobiliario-y-cesteria'
      },
      {
        image: '/images/invLarge/tinyImg/decohogar4.png', 
        url: 'https://www.decohogar.com.uy/catalogo?q=bodum'
      }
    ]   
  },
  {
    image: '/images/invLarge/nuda.jpg',
    url: 'https://www.nudaprop.com/',
    whatsapp: 'https://wa.me/+59892923232'
  },
  {
    image: '/images/invLarge/mobilarte.jpg',
    url: 'https://www.mobilart.com.uy/muebles',
    whatsapp: 'https://wa.me/+59897582591'
  },
  {
    image: '/images/invLarge/sinestesia.jpg',
    url: 'https://www.instagram.com/sinestesiauy/',
    whatsapp: 'https://wa.me/+59895827890'
  },
]

export const SmallImg = [
  {
    image: '/images/invSmall/paraca.jpg',
    url: 'https://www.gestionpacara.com/',
    whatsapp: 'https://wa.me/+59899800802'
  },
]