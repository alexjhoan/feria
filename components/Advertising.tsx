import { Carousel } from "antd";
import React from "react";
import {useTheme} from "../shared-components/Styles/ThemeHook";
import { LargeImg, SmallImg } from "./imgData";
import {LeftOutlined, RightOutlined, PhoneFilled} from '@ant-design/icons';

export default function Advertising() {
	const {theme} = useTheme()

	const clickFalse = (e:any) => {
		e.preventDefault()
	}

	const settings = {
		dots: false,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2500,
		pauseOnHover: false,
		arrows: true,
		nextArrow: <RightOutlined />,
    prevArrow: <LeftOutlined />,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: false
				}
			}
		]
	};

	return (
		<React.Fragment>
			<div className='rowBanner'>
				<img src="/images/invLarge/invBanner.jpg" />
			</div>
			<div className="innerText">
				<p className='title'><span>¡RENOVÁ</span> TUS ESPACIOS!</p>
				<p className="subTitle">Conocé la amplia propuesta que tenemos para vos.</p>
			</div>
			<div className="containerLarge">
				{
					LargeImg.map((inv: any, index:any) =>(
						<div className='containerInvLarge' key={index}>
							<div className="invLarge">
								{
									inv.slides ?
										<Carousel {...settings}>
											{
												inv.slides.map((slide: any, index: any)=>(
													<a href={slide.url ? slide.url : "#"} onClick={slide.url === '' && clickFalse} target='_blank'>
														<img src={slide.img} alt={slide.image} />
													</a>
												))
											}
										</Carousel>
									:
										<a href={inv.url ? inv.url : "#"} onClick={inv.url === '' && clickFalse} target='_blank'>
											<img src={inv.image} alt={inv.image} />
										</a>
								}
							</div>
							{
								inv.tinyAd  &&
								<div className="imgTiny">
								{
										inv.tinyAd.map((ad:any, index:any) => (
											<a href={ad.url ? ad.url : "#"} onClick={ad.url === '' && clickFalse} target='_blank' key={index} >
												<img src={ad.image} alt="tinyImg" />
											</a>
										))
									}
								</div>
							}
							<div className="shared">
								<a href={inv.url ? inv.url : "#"} className='invBtn' onClick={inv.url === '' && clickFalse} target='_blank'>Ver Ofertas</a>
								{
									inv.whatsapp &&
										<a href={inv.whatsapp} className='invBtn whatsApp' target='_blank'><img src="/images/logos/whatsapp.png" alt="whatsapp" />Enviar WhatsApp</a>
								}
							</div>
						</div>
					))
				}
			</div>
			<div className="containerSmall">
			{
					SmallImg.map((inv: any, index:any) =>(
						<div className="invSmall" key={index}>
							<a href={inv.url ? inv.url : '#'} onClick={inv.url === '' && clickFalse} target='_blank'>
								<img src={inv.image} alt={inv.image} />
							</a>
							<div className="shared">
							{
									inv.mail ?
										<a href={inv.mail} className='invBtn' onClick={inv.mail === '' && clickFalse} target='_blank'>Consultar</a>
									:
										<a href={inv.url} className='invBtn' onClick={inv.url === '' && clickFalse} target='_blank'>Ver Ofertas</a>
								}
								{
									inv.tel ?
										<a href={inv.tel} className='invBtn telefono' onClick={inv.tel === '' && clickFalse} target='_blank'><PhoneFilled style={{ fontSize: 'calc(1em + 2px)', color: '#fff', transform: 'scaleX(-1)', marginRight: 5, }} /> Llamar</a>
									:
										<a href={inv.whatsapp} className='invBtn whatsApp' onClick={inv.whatsapp === '' && clickFalse} target='_blank'><img src="/images/logos/whatsapp.png" alt="whatsapp" />Enviar WhatsApp</a>
								}
							</div>
						</div>
					))
				}
			</div>
			<style jsx global>{`
				.rowBanner{
					max-width: 1076px;
					margin: -15px auto 0;
					padding: 0 16px;
				}
				
				.rowBanner img{
					width: 100%;
					height: auto;
				}
				.innerText {
					padding: 80px 16px 60px;
				}
				p.title {
					font-size: 60px;
					font-weight: 700;
					font-family: Poppins;
					text-align: center;
					color: #003358;
					margin: 0 0 20px;
					line-height: 1.2;
				}
				p.title span{
					font-size: 60px;
					font-weight: 700;
					font-family: Poppins;
					color: #fa6416;
				}
				p.subTitle {
					font-size: 32px;
					font-weight: 500;
					font-family: Poppins;
					text-align: center;
					color: #003358;
					margin: 0;
				}
				.containerLarge {
					max-width: 1076px;
					margin: 0 auto;
					padding: 0 16px;
					display: grid;
					grid-template-columns: 100%;
					grid-row-gap: 50px;
				}
				.containerInvLarge, .invSmall{
					background-color: #ededf5;
					padding: 20px;
					border: solid 1px #b8b8b8;
					border-radius: 15px;
				}
				.invLarge{
					position: relative;
				}
				.invLarge > a img, .invLarge .ant-carousel a img {
					position: relative;
					width: 100%;
					height: auto;
					z-index: 1;
					box-shadow: 0px 0px 10px 0px #00000060;
					transition: all ease-in-out .2s;
				}
				.invLarge > a img:hover, .invLarge .ant-carousel a img{
					transform: translateY(-2px);
				}
				.invLarge .ant-carousel .slick-prev, .invLarge .ant-carousel .slick-next {
					z-index: 1;
					transform: translateY(-50%);
					top: 50%;
				}
				.invLarge .ant-carousel svg{
					width: 35px;
					height: 35px;
					color: #fff;
					filter: drop-shadow(2px 4px 2px #000);
				}
				.invLarge .ant-carousel .slick-prev {
					left: 25px;
				}
				.invLarge .ant-carousel .slick-next {
					right: 35px;
				}
				.invLarge .ant-carousel .slick-prev {
					left: 25px;
				}
				.invLarge .ant-carousel .slick-next {
					right: 35px;
				}
				.shared {
					display: flex;
					flex-direction: row;
					justify-content: center;
					margin-top: 15px;
				}
				.invBtn{
					background: #fa6416;
					padding: 5px 45px;
					font-size: 14px;
					border-radius: 7px;
					color: #fff;
					font-weight: 500;
					font-family: Poppins;
					box-shadow: 0px 0px 10px 0px #00000060;
					border: solid 2px #fff;
					display: flex;
					flex-direction: row;
					align-items: center;
					justify-content: center;
					height: 39px;
				}
				.invBtn:hover{
					color: #fff;
				}
				.telefono{
					background: #1890ff;
					margin-left: 8px;
				}
				.whatsApp{
					background: #00bb2d;
					margin-left: 8px;
				}
				.whatsApp img{
					height: 25px;
					margin-right: 5px;
				}
				.containerSmall {
					max-width: 1076px;
					margin: 0 auto;
					padding: 50px 16px;
					display: grid;
					grid-template-columns: 1fr 1fr;
					grid-row-gap: 50px;
					grid-column-gap: 20px;
				}
				.invSmall{
					position: relative;
				}
				.invSmall > a img{
					position: relative;
					width: 100%;
					height: auto;
					z-index: 1;
					box-shadow: 0px 0px 10px 0px #00000060;
					transition: all ease-in-out .2s;
				}
				.invSmall > a img:hover{
					transform: translateY(-2px);
				}
				.imgTiny {
					display: grid;
					grid-template-columns: repeat(3, 1fr);
					grid-column-gap: 20px;
					margin-top: 10px;
					grid-row-gap: 10px;
				}
				.imgTiny a img{
					width: 100%;
					height: auto;
					box-shadow: 0px 0px 10px 0px #00000060;
					transition: all ease-in-out .2s;
				}
				.imgTiny a img:hover{
					transform: translateY(-2px);
				}
				@media (min-width: ${theme.breakPoints.sm}) and (max-width: ${theme.breakPoints.lg}) {
					.invBtn{
						padding: 0;
						font-size: calc(4px + 1.5vw);
						border-radius: 5px;
						border: solid 1px #fff;
						box-shadow: 0px 0px 7px -1px #00000060;
						width: 40%;
						height: calc(20px + 1.5vw);
					}
					.containerSmall .shared .whatsApp{
						margin-left: 8px;
					}
					.whatsApp img{
						height: calc(6px + 1.5vw);
						transform: translateY(-1px);
					}
					.shared {
						position: relative;
						bottom: 0;
						left: 0;
						z-index: 2;
						display: flex;
						flex-direction: row;
						align-content: center;
						justify-content: center;
						padding-top: 3px;
						width: 100%;
  					column-gap: 10px;
					}
					.whatsApp, .telefono{
						margin-left: 0;
						width: 60%;
					}
				}
				@media (max-width: ${theme.breakPoints.xl}) {
					p.title {
						font-size: calc(32px + 1.5vw);
					}
					p.title span{
						font-size: calc(32px + 1.5vw);
					}
					p.subTitle {
						font-size: calc(14px + 1.5vw);
					}
					.innerText {
						padding: 50px 16px 40px;
					}
				}
				@media (min-width: ${theme.breakPoints.sm}) and (max-width: ${theme.breakPoints.md}) {
					.containerSmall {
						padding: 30px 16px 50px;
						grid-row-gap: 30px;
					}
					.containerSmall .shared .whatsApp{
						margin-left: 0;
					}
					.containerInvLarge, .invSmall{
						border-radius: 10px;
						padding: 10px;
					}
					.containerLarge {
						grid-row-gap: 30px;
					}
				}
				@media (max-width: ${theme.breakPoints.sm}) {
					.invLarge > a img, .invLarge .ant-carousel a img {
						box-shadow: 0px 0px 7px 0px #00000060;
					}
					.containerSmall {
						grid-template-columns: 1fr;
						grid-row-gap: 35px;
					}
					.invBtn{
						padding: 1.3vw calc(4px + 1.5vw);
						font-size: calc(7px + 1.3vw);
						border-radius: 5px;
						border: solid 1px #fff;
						box-shadow: 0px 0px 7px -1px #00000060;
						width: 40%;
						height: calc(25px + 1.5vw);
					}
					.shared {
						position: relative;
						bottom: 0;
						left: 0;
						z-index: 2;
						display: flex;
						flex-direction: row;
						align-content: center;
						justify-content: center;
						padding-top: 3px;
						width: 100%;
  					column-gap: 10px;
					}
					.whatsApp, .telefono{
						margin-left: 0;
						width: 60%;
					}
					.whatsApp img{
						height: calc(10px + 1.5vw);
						margin-right: 5px;
						transform: translateY(-1px);
					}
					.imgTiny {
						grid-template-columns: repeat(1, 1fr);
						grid-column-gap: 10px;
					}
					.containerInvLarge, .invSmall{
						border-radius: 7px;
						padding: 10px 7px;
					}
					.containerLarge {
						grid-row-gap: 30px;
					}
					.containerSmall {
						padding: 30px 10px 50px;
						grid-row-gap: 30px;
					}
					.containerLarge {
						padding: 0 10px;
					}
				}
			`}</style>
		</React.Fragment>
	);
}
